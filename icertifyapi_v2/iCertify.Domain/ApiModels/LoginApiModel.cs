﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iCertify.Domain.ApiModels
{
    public class LoginApiModel
    {
        [EmailAddress]
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
