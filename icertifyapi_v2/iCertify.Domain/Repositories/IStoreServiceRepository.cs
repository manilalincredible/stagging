﻿using iCertify.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iCertify.Domain.Repositories
{
    public interface IStoreServiceRepository : IDisposable
    {
        Task<bool> UpdateAsync(StoreService storeService, CancellationToken ct = new CancellationToken());
    }
}
