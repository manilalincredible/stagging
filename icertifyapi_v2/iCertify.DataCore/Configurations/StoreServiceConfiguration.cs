﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iCertify.DataCore.Configurations
{
    public class StoreServiceConfiguration
    {
        public StoreServiceConfiguration(EntityTypeBuilder<StoreService> entity)
        {
            entity.HasOne(d => d.Store)
                .WithMany(p => p.StoreService)
                .HasForeignKey(d => d.StoreId)
                .OnDelete(DeleteBehavior.Cascade);
      
        }
    }
}
