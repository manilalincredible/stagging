﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCertify.Domain.Exceptions
{
    public class InValidDataException : Exception
    {
        public InValidDataException(string message,object errorData)
        : base(message)
        {
            this.ErrorData = errorData;
        }

        public Object ErrorData { get; set; }
    }
}
