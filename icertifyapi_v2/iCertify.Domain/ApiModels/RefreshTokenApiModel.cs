﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCertify.Domain.ApiModels
{
    public class RefreshTokenApiModel
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}
