﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;


namespace iCertify.Api.MIddleWare
{
    public class FormatResponse
    {
        private readonly RequestDelegate _next;
        public FormatResponse(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            var currentBody = context.Response.Body;
            using (var memoryStream = new MemoryStream())
            {
                context.Response.Body = memoryStream;
                await _next(context);
                context.Response.Body = currentBody;
                memoryStream.Seek(0, SeekOrigin.Begin);
                var readToEnd = new StreamReader(memoryStream).ReadToEnd();
                var objResult = JsonConvert.DeserializeObject(readToEnd);
                var result = ApiResponse.Create((HttpStatusCode)context.Response.StatusCode, objResult, null);
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
            }
        }

    }

    public static class FormatResponseExtensions
    {
        public static IApplicationBuilder FormatResponse(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FormatResponse>();
        }
    }


    public class ApiResponse
    {
        public static ApiResponse Create(HttpStatusCode statusCode, object result = null, string errorMessage = null)
        {
            return new ApiResponse(statusCode, result, errorMessage);
        }

        public int StatusCode { get; set; }

        public string RequestId { get; }

        public string ErrorMessage { get; set; }

        public object Result { get; set; }

        protected ApiResponse(HttpStatusCode statusCode, object result = null, string errorMessage = null)
        {
            RequestId = Guid.NewGuid().ToString();
            StatusCode = (int)statusCode;
            Result = result;
            ErrorMessage = errorMessage;
        }
    }
}
