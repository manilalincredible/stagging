﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Api.Configurations;
using iCertify.Api.MIddleWare;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;
using iCertify.Domain.Service;
using iCertify.Domain.Utilities.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace iCertify.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StoreController : BaseController
    {
        private readonly AppSettings _appSettings;

        public StoreController(IiCertifyService iCertifyService, ILogger<StoreController> logger, AppSettings appSettings) : base(iCertifyService, logger)
        {
            _appSettings = appSettings;
        }


        [HttpGet]
        [Produces(typeof(List<StoreApiModel>))]
        public async Task<ActionResult> Get(CancellationToken ct = new CancellationToken())
        {
            return Ok(await _iCertifyService.GetAllStoreAsync(ct));
        }

        [HttpGet("{id}")]
        [Produces(typeof(StoreApiModel))]
        public async Task<ActionResult> Get(int id, CancellationToken ct = new CancellationToken())
        {

            var store = await _iCertifyService.GetStoreByIdAsync(id, ct);
            if (store == null)
            {
                return NotFound();
            }
            return Ok(store);

        }

        [HttpGet("company/{id}")]
        [Produces(typeof(List<StoreApiModel>))]
        public async Task<ActionResult> GetStoreByCompanyId(int id, CancellationToken ct = new CancellationToken())
        {

            return Ok(await _iCertifyService.GetStoreByCompanyIdAsync(id, ct));

        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Produces(typeof(StoreApiModel))]
        public async Task<ActionResult> Post([FromBody] StoreApiModel input, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    ErrorMessage = ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage)).ToList()
                });
            }

            var mailSettings = new MailSettings
            {
                From = _appSettings.EmailSettings.From,
                SmtpServer = _appSettings.EmailSettings.SmtpServer,
                Port = _appSettings.EmailSettings.Port,
                Username = _appSettings.EmailSettings.Username,
                Password = _appSettings.EmailSettings.Password
            };
            return StatusCode(201, await _iCertifyService.AddStoreAsync(input, mailSettings, ct));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<StoreApiModel>> Put(int id, [FromBody] StoreApiModel input, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
                return BadRequest();

            if (await _iCertifyService.GetStoreByIdAsync(id, ct) == null)
            {
                return NotFound();
            }
            if (await _iCertifyService.UpdateStoreAsync(input, ct))
            {
                return Ok(input);
            }
            return StatusCode(500);

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id, CancellationToken ct = new CancellationToken())
        {
            if (await _iCertifyService.GetStoreByIdAsync(id, ct) == null)
            {
                return NotFound();
            }

            if (await _iCertifyService.DeleteStoreAsync(id, ct))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        [HttpPost("registration")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<RegistrationResponse>> Registration([FromBody] StoreRegisterApiModel input, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    ErrorMessage = ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage)).ToList()
                });
            }
            return StatusCode(201, await _iCertifyService.RegisterStoreAsync(input, ct));
        }
    }
}