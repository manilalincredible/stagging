﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iCertify.DataCore.Configurations
{
    public class StoreConfiguration
    {
        public StoreConfiguration(EntityTypeBuilder<Store> entity)
        {
            entity.Property(e => e.Name).IsRequired().HasMaxLength(150);
            entity.Property(e => e.CreatedDate).HasDefaultValueSql("getDate()");

            entity.HasOne(d => d.Company)
                .WithMany(p => p.Stores)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasMany(d => d.StoreService)
                .WithOne(p => p.Store)
                .HasForeignKey(d => d.StoreId)
                .OnDelete(DeleteBehavior.Cascade);           
        }
    }
}
