﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.Entities;
using iCertify.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace iCertify.DataCore.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly iCertifyContext _context;

        public CompanyRepository(iCertifyContext context)
        {
            _context = context;
        }

        public async Task<List<Company>> GetAllAsync(CancellationToken ct = new CancellationToken())
        {
            return await _context.Companies.AsNoTracking().ToListAsync(ct);
        }

        public async Task<Company> GetByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return await _context.Companies.FindAsync(id);
        }

        public async Task<Company> AddAsync(Company newCompany, CancellationToken ct = new CancellationToken())
        {
            _context.Companies.Add(newCompany);
            await _context.SaveChangesAsync(ct);
            return newCompany;
        }

        public async Task<bool> UpdateAsync(Company company, CancellationToken ct = new CancellationToken())
        {
            if (!await CompanyExists(company.Id, ct))
                return false;
            _context.Companies.Update(company);
            await _context.SaveChangesAsync(ct);
            return true;
        }

        public async Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken())
        {
            if (!await CompanyExists(id, ct))
                return false;
            var toRemove = _context.Companies.Find(id);
            toRemove.IsActive = false;
            await _context.SaveChangesAsync(ct);
            return true;
        }

        public async Task<bool> CompanyExists(int id, CancellationToken ct = new CancellationToken()) =>
            await _context.Companies.AnyAsync(a => a.Id == id, ct);

        public async Task<Company> GetCompanyByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return await _context.Companies.Include(x => x.Services).Where(y => y.Id == id).FirstOrDefaultAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
