﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;
using iCertify.Domain.Exceptions;
using iCertify.Domain.Extensions;
using iCertify.Domain.Utilities;
using iCertify.Domain.Utilities.Model;
using iCertify.Domain.Constants;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace iCertify.Domain.Service
{
    public partial class iCertifyService
    {
        public async Task<IEnumerable<StoreApiModel>> GetAllStoreAsync(CancellationToken ct = new CancellationToken())
        {
            return (await _storeRepository.GetAllAsync(ct))?.ConvertAll();
        }

        public async Task<StoreApiModel> GetStoreByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return (await _storeRepository.GetByIdAsync(id, ct))?.Convert();
        }

        public async Task<IEnumerable<StoreApiModel>> GetStoreByCompanyIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return (await _storeRepository.GetByCompanyIdAsync(id, ct))?.ConvertAll();
        }

        public async Task<StoreApiModel> AddStoreAsync(StoreApiModel newStoreApiModel, MailSettings email, CancellationToken ct = new CancellationToken())
        {
            var newStore = newStoreApiModel.Convert();
            if (newStoreApiModel.StoreServices.Count == 0)
            {
                throw new InValidDataException(ValidationMessage.StoreServiceExist, newStoreApiModel);
            }

            var companyExits = await _companyRepository.CompanyExists(newStoreApiModel.CompanyId, ct);
            if (!companyExits)
            {
                throw new InValidDataException(ValidationMessage.InvalidComp, newStoreApiModel);
            }

            foreach (var storeService in newStore.StoreService)
            {
                if (!await _storeRepository.ServiceExists(storeService.ServiceId, newStore.CompanyId, ct))
                {
                    throw new InValidDataException(ValidationMessage.InvalidStore, newStoreApiModel);
                }
                storeService.Pin = this.GenerateBitRandomNumber((int)BitSize.Short);
            }

            newStore = await _storeRepository.AddAsync(newStore, ct);

            _logger.LogInformation("New Store Created With Id {0} , TimeStamp : {1}", newStore.Id, DateTime.UtcNow);

            //todo?  this need to be moved to a Queue.
            var companyServicesDetails = await _companyServiceRepository.GetByIdsAsync(newStore.StoreService.Select(x => x.ServiceId).ToList(), newStore.CompanyId, ct);
            foreach (var item in newStore.StoreService)
            {
                string svcName = string.Empty;
                if (companyServicesDetails.Count > 0)
                    svcName = companyServicesDetails.FirstOrDefault(x => x.Id == item.ServiceId).Name;
                Notifier.Send("Store Registration Pin", $"Please see your {svcName} registration pin : <b> { item.Pin} </b>", newStore.Email, email);
            }
            return newStore.Convert();
        }

        public async Task<bool> UpdateStoreAsync(StoreApiModel storeApiModel, CancellationToken ct = new CancellationToken())
        {
            var store = await _storeRepository.GetByIdAsync(storeApiModel.StoreId, ct);
            if (store is null) return false;
            store.Id = storeApiModel.StoreId;
            store.Name = storeApiModel.StoreName;
            return await _storeRepository.UpdateAsync(store, ct);
        }

        public async Task<bool> DeleteStoreAsync(int id, CancellationToken ct = new CancellationToken()) => await _storeRepository.DeleteAsync(id, ct);

        public async Task<RegistrationResponse> RegisterStoreAsync(StoreRegisterApiModel storeRegApiModel, CancellationToken ct = new CancellationToken())
        {
            var storeService = await _storeRepository.GetStoreServiceByPinAsync(storeRegApiModel.Pin, ct);
            if (storeService is null)
            {
                throw new InValidDataException(ValidationMessage.InvalidRegistrationPin, storeService);
            }
            else
            {               
                storeService.UniqueId = storeRegApiModel.UniqueId;
                storeService.PublicKey = storeRegApiModel.publickey;
                storeService.BitStrength = storeRegApiModel.BitStrength;
                storeService.KeyVersion = storeRegApiModel.KeyVersion;
                storeService.EffectiveStartDate = DateTime.Now;
                storeService.EffectiveEndDate = DateTime.Now.AddYears(_companyRepository.GetCompanyByIdAsync(storeService.Store.CompanyId, ct).Result.StoreExpiredLimit);                
                storeService.StoreGuid = Guid.NewGuid().ToString();
                storeService.IsActive = true;
                storeService.Store.Name = storeRegApiModel.StoreName == string.Empty ? storeService.Store.Name : storeRegApiModel.StoreName;
                storeService.Store.Phone = storeRegApiModel.StorePhone == string.Empty ? storeService.Store.Phone : storeRegApiModel.StoreName;
                storeService.Store.Email = storeRegApiModel.StoreEmail == string.Empty ? storeService.Store.Email : storeRegApiModel.StoreEmail;
                storeService.Store.UniqueId = storeRegApiModel.UniqueId == string.Empty ? storeService.Store.UniqueId : storeRegApiModel.UniqueId;
                storeService.Store.ModifiedDate = DateTime.Now;
                storeService.Store.IsActive = true;
            }
            if (!await _storeServiceRepository.UpdateAsync(storeService, ct))
            {
                throw new InValidDataException(ValidationMessage.RegistrationInernalErr, storeService);
            }
            var companyServicesDetails = await _companyServiceRepository.GetByIdsAsync(storeService.Store.StoreService.Select(x => x.ServiceId).ToList(), storeService.Store.CompanyId, ct);
            var storeResp = new RegistrationResponse
            {
                StoreId = storeService.Store.Id,
                CompanyId = storeService.Store.CompanyId,
                AccountNumber = storeService.Store.BankAccountNumber,
                RouteCode = storeService.Store.BankRouteCode,
                StoreGuid = storeService.StoreGuid,
                ServiceType = companyServicesDetails.Count == 0 ? 0 : companyServicesDetails.FirstOrDefault(x => x.Id == storeService.ServiceId).Type
            };
            return storeResp;
        }
    }
}
