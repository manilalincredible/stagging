﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Repositories;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace iCertify.Domain.Service
{
    public partial class iCertifyService : IiCertifyService
    {
        private readonly IStoreServiceRepository _storeServiceRepository;
        private readonly IStoreRepository _storeRepository;
        private readonly ICompanyServiceRepository _companyServiceRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IIdentityRepository _identityRepository;
        private readonly IMemoryCache _cache;
        private readonly TokenValidationParameters _tokenValidationParameters;
        private readonly ILogger _logger;        
        public iCertifyService()
        {
            
        }

        public iCertifyService(TokenValidationParameters tokenValidationParameters, ILogger<iCertifyService> logger, IMemoryCache memoryCache, IStoreRepository storeRepository, ICompanyRepository companyRepository, IIdentityRepository identityRepository, ICompanyServiceRepository companyServiceRepository,IStoreServiceRepository storeServiceRepository)
        {
            _logger = logger;
            _cache = memoryCache;
            _storeRepository = storeRepository;
            _companyRepository = companyRepository;
            _identityRepository = identityRepository;
            _companyServiceRepository = companyServiceRepository;
            _storeServiceRepository = storeServiceRepository;           
        }

    }
}
