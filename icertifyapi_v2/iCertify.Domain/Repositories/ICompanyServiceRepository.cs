﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iCertify.Domain.Repositories
{
    public interface ICompanyServiceRepository : IDisposable
    {
        Task<List<iCertify.Domain.Entities.Service>> GetByIdsAsync(List<int> ids, int companyId, CancellationToken ct = new CancellationToken());
    }
}
