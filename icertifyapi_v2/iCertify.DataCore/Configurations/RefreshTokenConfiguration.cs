﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iCertify.DataCore.Configurations
{
    public class RefreshTokenConfiguration
    {
        public RefreshTokenConfiguration(EntityTypeBuilder<RefreshToken> entity)
        {
            entity.Property(e => e.CreatedDate).HasDefaultValueSql("getDate()");
        }
    }
}
