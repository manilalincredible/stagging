﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Converters;

namespace iCertify.Domain.Entities
{
    public class Store : IConvertModel<Store, StoreApiModel>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }

        [MaxLength(15)]
        [Phone]
        public string Phone { get; set; }

        [MaxLength(100)]
        [EmailAddress]
        [Column(TypeName = "varchar(100)")]
        public string Email { get; set; }

        [MaxLength(50)]
        [Column(TypeName = "varchar(50)")]
        public string State { get; set; }

        [MaxLength(50)]
        [Column(TypeName = "varchar(50)")]
        public string City { get; set; }

        [MaxLength(10)]
        [Column(TypeName = "varchar(10)")]
        public string Zip { get; set; }

        [MaxLength(250)]
        public string BankRouteCode { get; set; }

        [MaxLength(250)]
        public string BankAccountNumber { get; set; }

        [MaxLength(100)]
        public string UniqueId { get; set; }

        [MaxLength(100)]
        public string Latitude { get; set; }

        [MaxLength(100)]
        public string Longitude { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public int CreatedBy { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ModifiedDate { get; set; }

        public int? MigrationId { get; set; }

        public int? MigrationType { get; set; } //MO==1 KH==2

        [NotMapped]
        public string CompanyName { get; set; }

        public virtual Company Company { get; set; }

        public IEnumerable<StoreService> StoreService { get; set; } = new HashSet<StoreService>();

        public StoreApiModel Convert() => new StoreApiModel
        {
            StoreId = Id,
            CompanyId = CompanyId,
            StoreName = Name,
            StoreAddress = Address,
            StorePhone = Phone,
            StoreEmail = Email,
            StoreState = State,
            StoreCity = City,
            StoreZip = Zip,
            StoreBankRouteCode = BankRouteCode,
            StoreBankAccountNumber = BankAccountNumber,
            StoreLatitude = Latitude,
            StoreLongitude = Longitude,
            StoreServices = StoreService.Select(x=>x.ServiceId).ToList()
        };
    }
}
