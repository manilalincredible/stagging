﻿using iCertify.Domain.Entities;
using iCertify.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace iCertify.DataCore.Repositories
{
    public class CompanyServiceRepository : ICompanyServiceRepository
    {
        private readonly iCertifyContext _context;

        public CompanyServiceRepository(iCertifyContext context)
        {
            _context = context;
        }

        public async Task<List<Service>> GetByIdsAsync(List<int> ids, int companyId, CancellationToken ct = default)
        {
            return await _context.Services.Where(x => ids.Any(ur => ur == x.Id) && x.CompanyId == companyId).AsNoTracking().ToListAsync();  
        }


        public void Dispose()
        {
            _context.Dispose();
        }


    }
}
