﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using iCertify.Api.MIddleWare;

namespace iCertify.Api.Controllers
{

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyController : BaseController
    {
        public CompanyController(IiCertifyService iCertifyService, ILogger<CompanyController> logger) : base(iCertifyService, logger)
        {

        }


        [HttpGet]
        [Produces(typeof(List<CompanyApiModel>))]
        public async Task<ActionResult> Get(CancellationToken ct = new CancellationToken())
        {
            return Ok(await _iCertifyService.GetAllCompanyAsync(ct));
        }

        [HttpGet("{id}")]
        [Produces(typeof(CompanyApiModel))]
        public async Task<ActionResult> Get(int id, CancellationToken ct = new CancellationToken())
        {
            var company = await _iCertifyService.GetCompanyByIdAsync(id, ct);
            if (company == null)
            {
                return NotFound();
            }
            return Ok(company);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Produces(typeof(CompanyApiModel))]
        public async Task<ActionResult> Post([FromBody] CompanyApiModel input, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    ErrorMessage = ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage)).ToList()
                });
            }
            return StatusCode(201, await _iCertifyService.AddCompanyAsync(input, ct));
        }

        [HttpPut("{id}")]
        [Produces(typeof(CompanyApiModel))]
        public async Task<ActionResult> Put(int id, [FromBody] CompanyApiModel input, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
                return BadRequest();
            if (await _iCertifyService.GetCompanyByIdAsync(id, ct) == null)
            {
                return NotFound();
            }
            input.CompanyId = id;
            if (await _iCertifyService.UpdateCompanyAsync(input, ct))
            {
                return Ok(input);
            }
            return StatusCode(500);
        }

        [HttpDelete("{id}")]
        [Produces(typeof(bool))]
        public async Task<ActionResult> DeleteAsync(int id, CancellationToken ct = new CancellationToken())
        {
            if (await _iCertifyService.GetCompanyByIdAsync(id, ct) == null)
            {
                return NotFound();
            }

            if (await _iCertifyService.DeleteCompanyAsync(id, ct))
            {
                return Ok(true);
            }
            return StatusCode(500);

        }
    }
}