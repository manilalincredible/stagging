﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.Entities;

namespace iCertify.Domain.Repositories
{
    public interface IIdentityRepository : IDisposable
    {
        Task<RefreshToken> GetByIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<List<RefreshToken>> GetByTokenAsync(string token, CancellationToken ct = new CancellationToken());
        Task<RefreshToken> AddAsync(RefreshToken newRefreshToken, CancellationToken ct = new CancellationToken());
        Task<bool> UpdateAsync(RefreshToken refreshToken, CancellationToken ct = new CancellationToken());
        Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken());
    }
}
