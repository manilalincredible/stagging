﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using iCertify.Domain.Constants;
using iCertify.Domain.Entities;

namespace iCertify.Domain.ApiModels
{
    public class CompanyApiModel
    {
        public CompanyApiModel()
        {
            CompanyServices = new List<CompanyServiceApiModel>();
        }

        public int CompanyId { get; set; }

        [Required(ErrorMessage = ValidationMessage.NameReq)]
        [MaxLength(100)]
        public string CompanyName { get; set; }

        [MaxLength(250)]
        public string CompanyAddress { get; set; }

        [Phone]
        [MaxLength(15)]
        public string CompanyPhone { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        public string CompanyEmail { get; set; }

        [MaxLength(100)]
        public string CompanyCountry { get; set; }

        [MaxLength(100)]
        public string CompanyState { get; set; }

        [MaxLength(100)]
        public string CompanyCity { get; set; }

        [MaxLength(10)]
        public string CompanyZip { get; set; }

        [MaxLength(250)]
        public string CompanyBankRouteCode { get; set; }

        [MaxLength(250)]
        public string CompanyBankAccountNumber { get; set; }

        [Required(ErrorMessage = ValidationMessage.CompanyStoreExpiredReq)]
        public int CompanyStoreExpiredLimit { get; set; }

        public List<CompanyServiceApiModel> CompanyServices { get; set; }

        public Company Convert()
        {
            var cmpSvc = new List<iCertify.Domain.Entities.Service>();
            foreach (var item in CompanyServices)
            {
                cmpSvc.Add(new Entities.Service
                {
                    Name = item.Name,
                    Type = item.Type
                });
            }

            return new Company
            {
                Id = CompanyId,
                Name = CompanyName,
                Address = CompanyAddress,
                Phone = CompanyPhone,
                Email = CompanyEmail,
                Country = CompanyCountry,
                State = CompanyState,
                City = CompanyCity,
                Zip = CompanyZip,
                BankRouteCode = CompanyBankRouteCode,
                BankAccountNumber = CompanyBankAccountNumber,
                StoreExpiredLimit = CompanyStoreExpiredLimit,
                IsActive = true,
                Services = cmpSvc
            };
        }
    }

    public class CompanyServiceApiModel
    {
        [Required(ErrorMessage = ValidationMessage.ServiceReq)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Type { get; set; }
    }
}
