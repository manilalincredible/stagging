﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iCertify.Domain.Entities
{
    public class Service
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ForeignKey(nameof(Company))]
        public int CompanyId { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        public int Type { get; set; }// mo==1,kh==2

        [Required]
        public bool IsActive { get; set; }

        public virtual Company Company { get; set; }

    }
}
