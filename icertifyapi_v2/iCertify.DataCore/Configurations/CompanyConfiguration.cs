﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iCertify.DataCore.Configurations
{
    public class CompanyConfiguration
    {
        public CompanyConfiguration(EntityTypeBuilder<Company> entity)
        {
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(150);

            entity.HasMany(d => d.Stores)
                .WithOne(p => p.Company)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.Property(e => e.CreatedDate).HasDefaultValueSql("getDate()");
            
        }
    }
}
