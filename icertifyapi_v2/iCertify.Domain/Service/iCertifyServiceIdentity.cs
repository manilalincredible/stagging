﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;
using iCertify.Domain.Constants;
using Microsoft.IdentityModel.Tokens;

namespace iCertify.Domain.Service
{
    public partial class iCertifyService
    {
        public async Task<AuthenticationResultApiModel> ValidateUser(LoginApiModel credentials, string secret, TimeSpan tokenLifeTime, CancellationToken ct = new CancellationToken())
        {
            if (credentials.UserName == "user@ivs.com" && credentials.Password == "password")
            {
                return await GenerateAuthenticationResult(1, secret, tokenLifeTime, ct);
            }
            return new AuthenticationResultApiModel
            {
                Success = false,
                ErrorMessage = new List<string>
                {
                    ValidationMessage.InvalidUser
                }
            };
        }

        public async Task<AuthenticationResultApiModel> RefreshToken(string secret, string token, string refreshToken, TimeSpan tokenLifeTime, CancellationToken ct = new CancellationToken())
        {
            //validating the token to make sure it has the same signature
            var validatedToken = GetClaimsPrincipalFromToken(token);

            if (validatedToken == null)
            {
                return new AuthenticationResultApiModel
                {
                    ErrorMessage = new List<string> { "Invalid Token" },
                    Success = false
                };
            }

            #region OptionalValidation_ToCheckIfTokenExpired

            var expiryDateUnix =
                long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);
            var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(expiryDateUnix);

            if (expiryDateTimeUtc > DateTime.UtcNow)
            {
                return new AuthenticationResultApiModel
                {
                    ErrorMessage = new List<string> { ValidationMessage.TokenExpired },
                    Success = false
                };
            }

            #endregion OptionalValidation_ToCheckIfTokenExpired

            var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

            var storedRefreshToken = (await _identityRepository.GetByTokenAsync(refreshToken, ct)).FirstOrDefault();

            if (storedRefreshToken == null || (DateTime.UtcNow > storedRefreshToken.ExpiredDate) || storedRefreshToken.Invalidated || storedRefreshToken.IsUsed || storedRefreshToken.JwtId != jti)
            {
                return new AuthenticationResultApiModel
                {
                    ErrorMessage = new List<string> { ValidationMessage.InvalidToken },
                    Success = false
                };
            }
            #region diffrent refresh token validations

            //if (DateTime.UtcNow > storedRefreshToken.ExpiredDate)
            //{
            //    return new AuthenticationResultApiModel
            //    {
            //        ErrorMessage = new List<string> { "Refresh Token dont exist" },
            //        Success = false
            //    };
            //}

            //if (DateTime.UtcNow > storedRefreshToken.ExpiredDate)
            //{
            //    return new AuthenticationResultApiModel
            //    {
            //        ErrorMessage = new List<string> { "Refresh token has expired" },
            //        Success = false
            //    };
            //}

            //if (storedRefreshToken.Invalidated)
            //{
            //    return new AuthenticationResultApiModel
            //    {
            //        ErrorMessage = new List<string> { "Refresh token has been invalidated" },
            //        Success = false
            //    };
            //}

            //if (storedRefreshToken.IsUsed)
            //{
            //    return new AuthenticationResultApiModel
            //    {
            //        ErrorMessage = new List<string> { "Refresh token has been used" },
            //        Success = false
            //    };
            //}

            //if (storedRefreshToken.JwtId != jti)
            //{
            //    return new AuthenticationResultApiModel
            //    {
            //        ErrorMessage = new List<string> { "Refresh token does not match JWT token" },
            //        Success = false
            //    };
            //}

            #endregion diffrent refresh token validations

            storedRefreshToken.IsUsed = true;
            var status = await _identityRepository.UpdateAsync(storedRefreshToken, ct);

            // get user from db
            //var userId = validatedToken.Claims.Single(x => x.Type == "Id").Value;

            return await GenerateAuthenticationResult(1, secret, tokenLifeTime, ct);

        }

        private async Task<AuthenticationResultApiModel> GenerateAuthenticationResult(int userId, string secret, TimeSpan tokenLifeTime, CancellationToken ct)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, "user@ivs.com"),
                    new Claim(JwtRegisteredClaimNames.Email, "user@ivs.com"),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("Id", "1"),
                    new Claim("role", "Admin")
                    //new Claim("Admin", "true"),
                }),
                Expires = DateTime.UtcNow.Add(tokenLifeTime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var refreshToken = this.GenerateBitRandomNumber((int)BitSize.Large);
            var newRefreshToken = new RefreshToken
            {
                JwtId = token.Id,
                UserId = userId,
                ExpiredDate = DateTime.UtcNow.AddDays(1),
                Token = refreshToken
            };

            await _identityRepository.AddAsync(newRefreshToken, ct);

            return new AuthenticationResultApiModel
            {
                Token = tokenHandler.WriteToken(token),
                RefreshToken = refreshToken,
                Success = true
            };
        }

        private ClaimsPrincipal GetClaimsPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var principal = tokenHandler.ValidateToken(token, _tokenValidationParameters, out var validatedToken);
                return !IsJwtWithValidSecurityAlgorithm(validatedToken) ? null : principal;
            }
            catch
            {
                return null;
            }
        }

        public string GenerateBitRandomNumber(int size)
        {
            var randomNumber = new byte[size];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
        {
            return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
                   jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCulture);
        }
    }
}
