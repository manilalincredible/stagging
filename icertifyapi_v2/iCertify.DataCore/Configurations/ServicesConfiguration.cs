﻿using System;
using System.Collections.Generic;
using System.Text;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iCertify.DataCore.Configurations
{
    public class ServicesConfiguration
    {
        public ServicesConfiguration(EntityTypeBuilder<Service> entity)
        {
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(150);

            entity.HasOne(d => d.Company)
                .WithMany(p => p.Services)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.Cascade);           
        }
    }
}
