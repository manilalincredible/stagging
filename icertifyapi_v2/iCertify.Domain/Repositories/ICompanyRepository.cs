﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;

namespace iCertify.Domain.Repositories
{
    public interface ICompanyRepository : IDisposable
    {
        Task<List<Company>> GetAllAsync(CancellationToken ct = new CancellationToken());
        Task<Company> GetByIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<Company> AddAsync(Company newCompany, CancellationToken ct = new CancellationToken());
        Task<bool> UpdateAsync(Company company, CancellationToken ct = new CancellationToken());
        Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken());
        Task<bool> CompanyExists(int id, CancellationToken ct = new CancellationToken());
        Task<Company> GetCompanyByIdAsync(int id, CancellationToken ct = new CancellationToken());
    }
}
