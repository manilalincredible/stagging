﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCertify.Domain.Utilities.Model
{
    public class MailSettings
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
