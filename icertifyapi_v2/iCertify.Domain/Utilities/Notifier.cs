﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Mail;
using iCertify.Domain.Utilities.Model;

namespace iCertify.Domain.Utilities
{
    public class Notifier
    {
        /// <summary>
        /// Communicate message through email. 
        /// </summary>
        /// <param name="MailSubject">Subject of the mail</param>
        /// <param name="MailMessage">Message content of the mail</param>
        /// <param name="MailTo">Mail send to whom</param>
        /// <returns></returns>
        public static bool Send(string MailSubject, string MailMessage, string MailTo, MailSettings MailInfo)
        {

            string MailUser = MailInfo.Username;
            string MailPass = MailInfo.Password;
            string MailFrom = MailInfo.From;
            int MailPortAddress = Convert.ToInt32(MailInfo.Port);
            string MailHostAddress = MailInfo.SmtpServer;

            // Credentials
            var credentials = new NetworkCredential(MailUser, MailPass);
            // Mail message
            var mail = new MailMessage()
            {
                From = new MailAddress(MailFrom),
                Subject = MailSubject,
                Body = MailMessage
            };
            mail.IsBodyHtml = true;
            mail.To.Add(new MailAddress(MailTo));
            // Smtp client
            var client = new SmtpClient()
            {
                Port = MailPortAddress,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = MailHostAddress,
                EnableSsl = true,
                Credentials = credentials
            };
            client.Send(mail);
            return true;
        }
    }
}
