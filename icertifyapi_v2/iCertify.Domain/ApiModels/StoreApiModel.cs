﻿using System;
using System.Collections.Generic;
using iCertify.Domain.Entities;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Transactions;
using iCertify.Domain.Constants;

namespace iCertify.Domain.ApiModels
{
    public class StoreApiModel
    {
        public StoreApiModel()
        {
            StoreServices = new List<int>();
        }

        public int StoreId { get; set; }

        [Required(ErrorMessage = ValidationMessage.CompanyReq)]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = ValidationMessage.NameReq)]
        [MaxLength(150)]
        public string StoreName { get; set; }

        [MaxLength(250)]
        public string StoreAddress { get; set; }

        [Phone]
        [MaxLength(15)]
        public string StorePhone { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        public string StoreEmail { get; set; }

        [MaxLength(50)]
        public string StoreState { get; set; }

        [MaxLength(50)]
        public string StoreCity { get; set; }

        [MaxLength(10)]
        public string StoreZip { get; set; }

        [MaxLength(250)]
        public string StoreBankRouteCode { get; set; }

        [MaxLength(250)]
        public string StoreBankAccountNumber { get; set; }

        [MaxLength(100)]
        public string StoreLatitude { get; set; }

        [MaxLength(100)]
        public string StoreLongitude { get; set; }

        public bool IsActive { get; set; }

        public List<int> StoreServices { get; set; }

        public Store Convert() => new Store
        {
            Id = StoreId,
            CompanyId = CompanyId,
            Name = StoreName,
            Address = StoreAddress,
            Phone = StorePhone,
            Email = StoreEmail,
            State = StoreState,
            City = StoreCity,
            Zip = StoreZip,
            BankRouteCode = StoreBankRouteCode,
            BankAccountNumber = StoreBankAccountNumber,
            Latitude = StoreLatitude,
            Longitude = StoreLongitude,
            CreatedDate = DateTime.Now,
            IsActive = IsActive,
            StoreService = (from item in StoreServices
                            select new StoreService
                            {
                                ServiceId = item
                            }).ToList()
        };
    }

    public class StoreRegisterApiModel
    {
        [MaxLength(150)]
        public string StoreName { get; set; }

        [Phone]
        [MaxLength(15)]
        public string StorePhone { get; set; }

        [EmailAddress]
        [MaxLength(100)]
        public string StoreEmail { get; set; }

        [Required(ErrorMessage = ValidationMessage.RegistrationPinReq)]
        [MaxLength(15)]
        public string Pin { get; set; }

        [Required(ErrorMessage = ValidationMessage.UniqueIdReq)]
        [MaxLength(250)]
        public string UniqueId { get; set; }

        [Required(ErrorMessage = ValidationMessage.PublicKeyReq)]
        public string publickey { get; set; }

        [Required(ErrorMessage = ValidationMessage.KeyVersionReq)]
        [MaxLength(5)]
        public string KeyVersion { get; set; }

        [Required(ErrorMessage = ValidationMessage.BitStrengthReq)]
        public int BitStrength { get; set; }
    }

    public class RegistrationResponse
    {
        public int CompanyId { get; set; }

        public int StoreId { get; set; }

        public string StoreGuid { get; set; }

        public string RouteCode { get; set; }

        public string AccountNumber { get; set; }

        public int ServiceType { get; set; } //1-MO, 2-KH
    }
}
