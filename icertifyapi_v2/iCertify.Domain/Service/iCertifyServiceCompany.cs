﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Exceptions;
using iCertify.Domain.Extensions;
using iCertify.Domain.Constants;

namespace iCertify.Domain.Service
{
    public partial class iCertifyService
    {
        public async Task<IEnumerable<CompanyApiModel>> GetAllCompanyAsync(CancellationToken ct = new CancellationToken())
        {
            return (await _companyRepository.GetAllAsync(ct))?.ConvertAll();
        }

        public async Task<CompanyApiModel> GetCompanyByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return (await _companyRepository.GetByIdAsync(id, ct))?.Convert();
        }

        public async Task<CompanyApiModel> AddCompanyAsync(CompanyApiModel newCompanyApiModel, CancellationToken ct = new CancellationToken())
        {
            if (newCompanyApiModel.CompanyServices.Count == 0)
            {
                throw new InValidDataException(ValidationMessage.CompanyServiceExist, newCompanyApiModel);
            }

            var newCompany = newCompanyApiModel.Convert();
            newCompany = await _companyRepository.AddAsync(newCompany, ct);
            return newCompany.Convert();
        }

        public async Task<bool> UpdateCompanyAsync(CompanyApiModel companyApiModel, CancellationToken ct = new CancellationToken())
        {
            var company = await _companyRepository.GetByIdAsync(companyApiModel.CompanyId, ct);
            if (company is null) return false;
            company.Id = companyApiModel.CompanyId;
            company.Name = companyApiModel.CompanyName;
            return await _companyRepository.UpdateAsync(company, ct);
        }

        public async Task<bool> DeleteCompanyAsync(int id, CancellationToken ct = new CancellationToken())
            => await _companyRepository.DeleteAsync(id, ct);
    }
}
