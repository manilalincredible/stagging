﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;
using iCertify.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace iCertify.DataCore.Repositories
{
    public class StoreRepository : IStoreRepository
    {
        private readonly iCertifyContext _context;

        public StoreRepository(iCertifyContext context)
        {
            _context = context;
        }

        public async Task<List<Store>> GetAllAsync(CancellationToken ct = new CancellationToken())
        {
            return await _context.Stores.Include(x => x.Company).AsNoTracking().ToListAsync(ct);
        }

        public async Task<Store> GetByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return await _context.Stores.Include(x => x.Company).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<Store>> GetByCompanyIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            return await _context.Stores.
                 Include(x => x.Company).
                 Where(a => a.CompanyId == id).AsNoTracking().ToListAsync();
        }

        public async Task<Store> AddAsync(Store newStore, CancellationToken ct = new CancellationToken())
        {
            _context.Stores.Add(newStore);
            await _context.SaveChangesAsync(ct);
            return newStore;
        }

        public async Task<bool> UpdateAsync(Store store, CancellationToken ct = new CancellationToken())
        {
            if (!await StoreExists(store.Id, ct))
                return false;
            _context.Stores.Update(store);            
            await _context.SaveChangesAsync(ct);
            return true;
        }

        public async Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken())
        {
            if (!await StoreExists(id, ct))
                return false;
            var toRemove = _context.Stores.Find(id);
            toRemove.IsActive = false;
            await _context.SaveChangesAsync(ct);
            return true;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        private async Task<bool> StoreExists(int id, CancellationToken ct = new CancellationToken()) =>
            await _context.Stores.AnyAsync(a => a.Id == id, ct);


        public async Task<List<StoreService>> GetPinByIdAsync(int storeid, int serviceid, CancellationToken ct = new CancellationToken())
        {
            return await _context.StoreServices.
                 Where(a => a.StoreId == storeid && a.ServiceId == serviceid).AsNoTracking().ToListAsync();
        }

        public async Task<bool> ServiceExists(int serviceid, int companyid, CancellationToken ct = new CancellationToken()) =>
           await _context.Services.AnyAsync(a => a.Id == serviceid && a.CompanyId == companyid, ct);

        public async Task<List<Service>> GetServiceByIdAsync(int id, int companyid, CancellationToken ct = new CancellationToken())
        {
            return await _context.Services.
                 Where(a => a.Id == id && a.CompanyId == companyid).AsNoTracking().ToListAsync();
        }

        public async Task<Store> GetByPinAsync(string Pin, CancellationToken ct = new CancellationToken())
        {
            var strSvc = await _context.StoreServices.Where(x => x.Pin == Pin).AsNoTracking().ToListAsync();

            return await _context.Stores.Include(x => x.Company).Where(x => x.Id == strSvc[0].StoreId).FirstOrDefaultAsync();
        }

        public async Task<StoreService> GetStoreServiceByPinAsync(string Pin, CancellationToken ct = new CancellationToken())
        {
            return await _context.StoreServices.Include(x => x.Store).Where(x => x.Pin == Pin).FirstOrDefaultAsync();           
        }    
    }
}
