﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iCertify.Domain.Entities
{
    public enum ServiceType
    {
        MoneyOrder = 1,
        Voucher = 2
    }

    public enum BitSize
    {
        Short = 8,
        Medium = 16,
        Large = 32
    }
}
