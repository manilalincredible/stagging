﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iCertify.Api.Configurations;
using iCertify.Domain.Service;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace iCertify.Api.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    public class BaseController : ControllerBase
    {
        protected readonly IiCertifyService _iCertifyService;       
        protected readonly ILogger _logger;

        public BaseController(IiCertifyService iCertifyService, ILogger<BaseController> logger)
        {
            _iCertifyService = iCertifyService;
            _logger = logger;
        }
    }
}