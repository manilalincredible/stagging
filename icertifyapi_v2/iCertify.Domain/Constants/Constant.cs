﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace iCertify.Domain.Constants
{
    public static class ValidationMessage
    {
        public const string CompanyServiceExist = "Company cannot be created : Company require atlease one service";
        public const string StoreCanotCreate = "Store cannot be created";
        public const string StoreServiceExist = StoreCanotCreate + " : Store require atleast one service";
        public const string InvalidComp = StoreCanotCreate + " : Invalid Company";
        public const string InvalidStore = StoreCanotCreate + " : Invalid Store Service";
        public const string InvalidUser = "Invalid User";
        public const string InvalidToken = "Invalid Token";
        public const string TokenExpired = "This Token hasn't expired";
        public const string RegistrationFailed = "Registration failed";
        public const string RegistrationStoreExist = RegistrationFailed + " : Store does not exist";
        public const string RegistrationServiceExist = RegistrationFailed + " : Service does not exist";
        public const string RegistrationInernalErr = RegistrationFailed + " : Internal error";
        public const string NameReq = "Name is required";
        public const string ServiceReq = "Service is required";
        public const string CompanyReq = "Company is required";
        public const string RegistrationPinReq = "Registration pin is required";
        public const string UniqueIdReq = "Uniqueid is required";
        public const string PublicKeyReq = "Publick key is required";
        public const string KeyVersionReq = "Key version is required";
        public const string BitStrengthReq = "BitStrength is required";
        public const string InvalidRegistrationPin = "Invalid registration pin";
        public const string CompanyStoreExpiredReq = "Store expired limit required";
    }
}
