﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iCertify.Domain.Converters;

namespace iCertify.Domain.Extensions
{
    public static class ConvertExtensions
    {
        public static IEnumerable<TTarget> ConvertAll<TSource, TTarget>(
            this IEnumerable<IConvertModel<TSource, TTarget>> values)
            => values.Select(value => value.Convert());
    }
}
