﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Converters;
using System.Linq;

namespace iCertify.Domain.Entities
{
    public class Company : IConvertModel<Company, CompanyApiModel>
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Address { get; set; }

        [MaxLength(15)]
        [Column(TypeName = "varchar(15)")]
        public string Phone { get; set; }

        [MaxLength(100)]
        [Column(TypeName = "varchar(100)")]
        public string Email { get; set; }

        [MaxLength(100)]
        [Column(TypeName = "varchar(100)")]
        public string Country { get; set; }

        [MaxLength(100)]
        [Column(TypeName = "varchar(100)")]
        public string State { get; set; }

        [MaxLength(100)]
        [Column(TypeName = "varchar(100)")]
        public string City { get; set; }

        [MaxLength(10)]
        [Column(TypeName = "varchar(10)")]
        public string Zip { get; set; }

        [MaxLength(250)]
        public string BankRouteCode { get; set; }

        [MaxLength(250)]
        public string BankAccountNumber { get; set; }

        [Required]
        public int StoreExpiredLimit { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public int CreatedBy { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ModifiedDate { get; set; }

        public int? MigrationId { get; set; }

        public int? MigrationType { get; set; } //MO==1 KH==2

        public ICollection<Service> Services { get; set; } = new HashSet<Service>();

        public ICollection<Store> Stores { get; set; } = new HashSet<Store>();

        public CompanyApiModel Convert() =>
        new CompanyApiModel
        {
            CompanyId = Id,
            CompanyName = Name,
            CompanyAddress = Address,
            CompanyPhone = Phone,
            CompanyEmail = Email,
            CompanyCountry = Country,
            CompanyState = State,
            CompanyCity = City,
            CompanyZip = Zip,
            CompanyServices = Services != null ? (from service in Services
                                                  select new CompanyServiceApiModel
                                                  {
                                                      Id = service.Id,
                                                      Name = service.Name,
                                                      Type = service.Type
                                                  }).ToList() : null,
            CompanyBankRouteCode = BankRouteCode,
            CompanyBankAccountNumber = BankAccountNumber,
            CompanyStoreExpiredLimit = StoreExpiredLimit
        };

    }
}
