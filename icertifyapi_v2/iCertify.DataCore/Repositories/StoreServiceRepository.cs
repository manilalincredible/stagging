﻿using iCertify.Domain.Entities;
using iCertify.Domain.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace iCertify.DataCore.Repositories
{
    public class StoreServiceRepository : IStoreServiceRepository
    {

        private readonly iCertifyContext _context;

        public StoreServiceRepository(iCertifyContext context)
        {
            _context = context;
        }

        public async Task<bool> UpdateAsync(StoreService storeService, CancellationToken ct = new CancellationToken())
        {
            _context.StoreServices.Update(storeService);            
            await _context.SaveChangesAsync(ct);
            return true;
        }


        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
