﻿using System;
using iCertify.DataCore.Configurations;
using iCertify.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace iCertify.DataCore
{
    public class iCertifyContext : DbContext
    {
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<StoreService> StoreServices { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }


        public iCertifyContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new CompanyConfiguration(modelBuilder.Entity<Company>());
            new StoreConfiguration(modelBuilder.Entity<Store>());
            new ServicesConfiguration(modelBuilder.Entity<Service>());
            new StoreServiceConfiguration(modelBuilder.Entity<StoreService>());
            new RefreshTokenConfiguration(modelBuilder.Entity<RefreshToken>());
        }
    }
}
