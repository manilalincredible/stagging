﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iCertify.Domain.Entities
{
    public class StoreService
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey(nameof(Store))]
        public int StoreId { get; set; }

        [NotMapped]
        public string StoreName { get; set; }

        //[ForeignKey("Service")]
        public int ServiceId { get; set; }

        [MaxLength(15)]
        [Column(TypeName = "varchar(15)")]
        public string Pin { get; set; }

        public string UniqueId { get; set; }

        [MaxLength(200)]
        public string StoreGuid { get; set; }

        [MaxLength(5)]
        [Column(TypeName = "varchar(5)")]
        public string KeyVersion { get; set; }

        [MaxLength]
        public string PublicKey { get; set; }

        [MaxLength]
        public string PrivateKey { get; set; }

        public int? BitStrength { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? EffectiveStartDate { get; set; }

        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? EffectiveEndDate { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public virtual Store Store { get; set; }
    }
}
