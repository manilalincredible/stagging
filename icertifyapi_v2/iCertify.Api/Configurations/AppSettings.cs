﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iCertify.Api.Configurations
{
    public class AppSettings
    {
        public EmailSettings EmailSettings { get; set; }
    }



    public class EmailSettings
    {
        public string From { get; set; }
        public string SmtpServer { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
