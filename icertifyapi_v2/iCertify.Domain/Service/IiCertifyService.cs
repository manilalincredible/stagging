﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;
using iCertify.Domain.Utilities.Model;

namespace iCertify.Domain.Service
{
    public interface IiCertifyService
    {
        Task<IEnumerable<StoreApiModel>> GetAllStoreAsync(CancellationToken ct = new CancellationToken());
        Task<StoreApiModel> GetStoreByIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<IEnumerable<StoreApiModel>> GetStoreByCompanyIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<StoreApiModel> AddStoreAsync(StoreApiModel newStoreApiModel, MailSettings emailSettings,  CancellationToken ct = new CancellationToken());
        Task<bool> UpdateStoreAsync(StoreApiModel storeApiModel, CancellationToken ct = new CancellationToken());
        Task<bool> DeleteStoreAsync(int id, CancellationToken ct = new CancellationToken());
        Task<RegistrationResponse> RegisterStoreAsync(StoreRegisterApiModel storeRegApiModel, CancellationToken ct = new CancellationToken());

        Task<IEnumerable<CompanyApiModel>> GetAllCompanyAsync(CancellationToken ct = new CancellationToken());
        Task<CompanyApiModel> GetCompanyByIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<CompanyApiModel> AddCompanyAsync(CompanyApiModel newCompanyApiModel,CancellationToken ct = new CancellationToken());
        Task<bool> UpdateCompanyAsync(CompanyApiModel companyApiModel, CancellationToken ct = new CancellationToken());
        Task<bool> DeleteCompanyAsync(int id, CancellationToken ct = new CancellationToken());

        Task<AuthenticationResultApiModel> ValidateUser(LoginApiModel credentials, string secret, TimeSpan tokenLifeTime, CancellationToken ct);
        Task<AuthenticationResultApiModel> RefreshToken(string secret, string token, string refreshToken, TimeSpan tokenLifeTime, CancellationToken ct);

        string GenerateBitRandomNumber(int size);
    }
}
