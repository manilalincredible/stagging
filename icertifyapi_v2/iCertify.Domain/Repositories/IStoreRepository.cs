﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Entities;

namespace iCertify.Domain.Repositories
{
    public interface IStoreRepository : IDisposable
    {
        Task<List<Store>> GetAllAsync(CancellationToken ct = new CancellationToken());
        Task<Store> GetByIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<List<Store>> GetByCompanyIdAsync(int id, CancellationToken ct = new CancellationToken());
        Task<Store> AddAsync(Store newStore, CancellationToken ct = new CancellationToken());
        Task<bool> UpdateAsync(Store store, CancellationToken ct = new CancellationToken());
        Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken());
        Task<List<StoreService>> GetPinByIdAsync(int storeid, int serviceid, CancellationToken ct = new CancellationToken());
        Task<bool> ServiceExists(int serviceid, int companyid, CancellationToken ct = new CancellationToken());
        Task<Store> GetByPinAsync(string Pin, CancellationToken ct = new CancellationToken());
        Task<StoreService> GetStoreServiceByPinAsync(string Pin, CancellationToken ct = new CancellationToken());        
    }
}
