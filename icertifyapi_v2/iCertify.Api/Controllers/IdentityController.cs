﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Api.Configurations;
using iCertify.Domain.ApiModels;
using iCertify.Domain.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace iCertify.Api.Controllers
{
    public class IdentityController : BaseController
    {
        private readonly JwtSettings _jwtSettings;

        public IdentityController(IiCertifyService iCertifyService, ILogger<IdentityController> logger, JwtSettings jwtSettings) : base(iCertifyService, logger)
        {
            _jwtSettings = jwtSettings;
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginApiModel cred, CancellationToken ct = new CancellationToken())
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    ErrorMessage = ModelState.Values.SelectMany(x => x.Errors.Select(y => y.ErrorMessage)).ToList()
                });
            }
            var response = await _iCertifyService.ValidateUser(cred, _jwtSettings.Secret, _jwtSettings.TokenLifeTime, ct);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }

        [HttpPost("refresh")]
        public async Task<ActionResult> Refresh([FromBody] RefreshTokenApiModel refreshRequest, CancellationToken ct = new CancellationToken())
        {
            var response = await _iCertifyService.RefreshToken(_jwtSettings.Secret, refreshRequest.Token, refreshRequest.RefreshToken, _jwtSettings.TokenLifeTime, ct);
            if (response.Success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }
    }
}