﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using iCertify.Domain.Entities;
using iCertify.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace iCertify.DataCore.Repositories
{
    public class IdentityRepository : IIdentityRepository
    {
        private readonly iCertifyContext _context;

        public IdentityRepository(iCertifyContext context)
        {
            _context = context;
        }

        public async Task<RefreshToken> GetByIdAsync(int id, CancellationToken ct = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public async Task<List<RefreshToken>> GetByTokenAsync(string token, CancellationToken ct = new CancellationToken())
        {
            return await _context.RefreshTokens.
                Where(a => a.Token == token).AsNoTracking().ToListAsync();
        }

        public async Task<RefreshToken> AddAsync(RefreshToken newRefreshToken, CancellationToken ct = new CancellationToken())
        {
            _context.RefreshTokens.Add(newRefreshToken);
            await _context.SaveChangesAsync(ct);
            return newRefreshToken;
        }

        public async Task<bool> UpdateAsync(RefreshToken refreshToken, CancellationToken ct = new CancellationToken())
        {
            if (!await RefreshTokenExists(refreshToken.Id, ct))
                return false;
            _context.RefreshTokens.Update(refreshToken);
            await _context.SaveChangesAsync(ct);
            return true;
        }

        public Task<bool> DeleteAsync(int id, CancellationToken ct = new CancellationToken())
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        private async Task<bool> RefreshTokenExists(int id, CancellationToken ct = new CancellationToken()) =>
            await _context.RefreshTokens.AnyAsync(a => a.Id == id, ct);

    }
}
